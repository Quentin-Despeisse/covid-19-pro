let deferredPrompt;

window.addEventListener('beforeinstallprompt', (e) => {
    e.preventDefault();

    deferredPrompt = e;

    deferredPrompt.prompt();
    
    deferredPrompt.userChoice.then((choiceResult) => {
        if (choiceResult.outcome === 'accepted') {
            console.log('User Accepted the A2HS');
        } else {
            console.log('User Dismissed the A2HS');
        }
        deferredPrompt = null;
    });
});