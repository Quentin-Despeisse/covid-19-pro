function doVibrate() {
    console.log('Hello');
    navigator.vibrate(200);
}


function startVideo() {
    var video = document.getElementById('video');
  // Get access to the camera!
  if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      // Not adding `{ audio: true }` since we only want video now
      navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
          video.srcObject = stream;
          video.play();
      });
  }
  
  }
  
  function takePicture() {
    // Elements for taking the snapshot
    var canvas = document.getElementById('canvas');
    var context = canvas.getContext('2d');
    var video = document.getElementById('video');
  
    context.drawImage(video, 0, 0, 640, 480);
  }
  
  function stopVideo() {
    var video = document.getElementById('video');
    video.pause();
  }
  

function getLocation() {
    if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        console.log("Geo Location not supported by browser");
    }
}

window.onload = function(){
    getLocation();
};

function showPosition(position) {
    let location = {
        longitude: position.coords.longitude,
        latitude: position.coords.latitude
    };
    let map = L.map('map').setView([location.latitude, location.longitude], 11);
    let marker = L.marker([location.latitude, location.longitude]).addTo(map);
    L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
        minZoom: 1,
        maxZoom: 20
    }).addTo(map);
}

$(document).ready(function () {
    $("#shareButton").click(function () {
        if (navigator.share) {
            navigator.share({
                title: 'Pandem, Inc.',
                text: 'Check out the new Covid 19 Pro !',
                url: 'https://quentin-despeisse.gitlab.io/covid-19-pro/',
            })
                .then(() => console.log('Successful share'))
                .catch((error) => $("#shareButton").disable())
        }
    })
});

if (window.DeviceOrientationEvent) {
    window.addEventListener("deviceorientation", process, false);
} else {
    console.log("Device doesn't support Orientation feature");
}

function process(event) {
    var alpha = event.alpha;
    var beta = event.beta;
    var gamma = event.gamma;
    document.getElementById("log").innerHTML = "<ul style='list-style: none'><li>Alpha : " + alpha + "</li><li>Beta : " + beta + "</li><li>Gamma : " + gamma + "</li></ul>";
}
