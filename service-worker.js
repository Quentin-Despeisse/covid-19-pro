'use-strict';

const CACHE_NAME = 'pandem-inc-cache';

const FILES_TO_CACHE = [
    './',
    './index.html',
    './compare.html',
    './gallery.html',
    './shop.html',
    './assets/img/corona-cell-1080.png',
    './assets/img/covid19.png',
    './assets/img/corona-cell.png',
    './assets/img/covid18.png',
    './assets/img/covid18pro.png',
    './assets/img/grippe.png',
    './assets/img/rhume.png',
    './assets/icons/pandeminc128.png',
    './assets/icons/pandeminc144.png',
    './assets/icons/pandeminc152.png',
    './assets/icons/pandeminc192.png',
    './assets/icons/pandeminc256.png',
    './assets/icons/pandeminc512.png',
    './assets/compare.png',
    './assets/gallery.png',
    './assets/home.png',
    './assets/shop.png',
    './lib/bootstrap.min.css',
    './lib/bootstrap.min.js',
    './lib/jquery.min.js',
    './lib/popper.min.js'
];

self.addEventListener('install', (evt) => {
    console.log('[ServiceWorker] Install');

    evt.waitUntil(
        caches.open(CACHE_NAME).then((cache) => {
            return cache.addAll(FILES_TO_CACHE);
        })
    );
    self.skipWaiting();
});

self.addEventListener('activate', (evt) => {
    evt.waitUntil(
        caches.keys().then((keyList) => {
            return Promise.all(keyList.map((key) => {
                if (key !== CACHE_NAME) {
                    return caches.delete(key);
                }
            }));
        })
    );
    self.clients.claim();
});

self.addEventListener('fetch', (evt) => {
    if (evt.request.mode !== 'navigate') {
        return;
    }
    evt.respondWith(
        fetch(evt.request)
        .catch(() => {
            return caches.open(CACHE_NAME)
            .then((cache) => {
                return cache.match('index.html');
            });
        })
    );
});